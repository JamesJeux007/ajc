﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyEtHeritage
{
    class Client
    {
        //attributs
        public string CIN { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }

        private string tel;

        public string Tel
        {
            get { return tel; }
            set
            {
                //si le numéro de téléphone n'est pas renseigné, on l'indique
                if (value == "")
                {
                    tel = "Non renseigné";
                }
                else
                {
                    tel = value;
                }
            }
        }


        //constructeurs
        public Client(string cin, string nom, string prenom, string tel)
        {
            CIN = cin;
            Nom = nom;
            Prenom = prenom;
            Tel = tel;
        }

        public Client(string cin, string nom, string prenom)
        {
            CIN = cin;
            Nom = nom;
            Prenom = prenom;
            Tel = "";
        }

        //affichage des informations du client
        public void Afficher()
        {
            Console.WriteLine("CIN : {0}", CIN);
            Console.WriteLine("NOM : {0}", Nom);
            Console.WriteLine("Prénom : {0}", Prenom);
            Console.WriteLine("Tél : {0}", Tel);
        }
    }
}
