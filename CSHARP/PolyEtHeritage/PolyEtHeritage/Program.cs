﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyEtHeritage
{
    class Program
    {
        static void Main(string[] args)
        {
            //création du premier compte
            Compte premierCompte = new Compte(new Client("EE111222", "Salim", "Omar", "0611111"));

            //affichage des détails à l'instanciation
            premierCompte.ResumeCompte();
            //dépot
            premierCompte.Crediter(5000);
            premierCompte.ResumeCompte();
            //retrait
            premierCompte.Debiter(1000);
            premierCompte.ResumeCompte();

            //création du deuxième compte
            Compte deuxiemeCompte = new Compte(new Client("EE333444", "Karimi", "Samir", "0622222"));

            //affichage des détails à l'instanciation
            deuxiemeCompte.ResumeCompte();
            //crédit et débit
            deuxiemeCompte.Crediter(3000, premierCompte);
            premierCompte.Debiter(1000, deuxiemeCompte);
            //affichage des détails des deux comptes après opérations
            premierCompte.ResumeCompte();
            deuxiemeCompte.ResumeCompte();

            //affichage du nombre de comptes créés
            Compte.NbComptes();

            //fin de l'application
            Console.ReadLine();
        }
    }
}
