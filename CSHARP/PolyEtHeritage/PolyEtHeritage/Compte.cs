﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyEtHeritage
{
    class Compte
    {
        //variable contenant le nombre de compte créés
        private static int nbComptes = 0;

        //attributs
        public int NumCompte { get; private set; }
        public int Solde { get; private set; }
        public Client Proprietaire { get; set; }


        //constructeur
        public Compte(Client proprietaire)
        {
            Proprietaire = proprietaire;

            //numéro de compte généré automatiquement avec incrémentation du nombre de comptes actifs
            NumCompte = ++nbComptes;
            //solde de départ à zéro
            Solde = 0;
        }

        //méthodes permettant de créditer le compte
        public void Crediter(int montant)
        {
            Solde += montant;
            Console.WriteLine("{0} ont bien été crédités au compte {1}", montant, NumCompte);
        }

        public void Crediter(int montant, Compte compteDebit)
        {
            Solde += montant;
            compteDebit.Solde -= montant;
            Console.WriteLine("{0} ont bien été crédités au compte {1} et débités du compte {2}", montant, NumCompte, compteDebit.NumCompte);

        }

        //méthodes permattant de débiter le compte
        public void Debiter(int montant)
        {
            Solde -= montant;
            Console.WriteLine("{0} ont bien été débités du compte {1}", montant, NumCompte);
        }

        public void Debiter(int montant, Compte compteCredit)
        {
            Solde -= montant;
            compteCredit.Solde += montant;
            Console.WriteLine("{0} ont bien été débités du compte {1} et crédités au compte {2}", montant, NumCompte, compteCredit.NumCompte);
        }

        //affichage des informations du compte
        public void ResumeCompte()
        {
            //séparateur pour l'interface
            string separateur = "************************";

            Console.WriteLine();
            Console.WriteLine(separateur);
            Console.WriteLine("Numéro de compte : {0}", NumCompte);
            Console.WriteLine("Solde de compte : {0}", Solde);
            Console.WriteLine("Propiétaire du compte :");
            Proprietaire.Afficher();
            Console.WriteLine(separateur);
            Console.WriteLine();
        }

        //affichage du nombre de comptes
        public static void NbComptes()
        {
            Console.WriteLine("Nombre de comptes créés : {0}", nbComptes);
        }
    }
}
