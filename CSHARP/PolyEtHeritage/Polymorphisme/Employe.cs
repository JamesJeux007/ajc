﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphisme
{
    class Employe : Personne
    {
        //attributs
        public int Salaire { get; set; }

        //constructeur
        public Employe(string nom, string prenom, string dateDeNaissance, int salaire)
            : base(nom, prenom, dateDeNaissance)
        {
            Salaire = salaire;
        }

        //affichage des données de l'employé
        public override string Afficher()
        {
            return $"{base.Afficher()} / Salaire : {Salaire}";
        }
    }
}
