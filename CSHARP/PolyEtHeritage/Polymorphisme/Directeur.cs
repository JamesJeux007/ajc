﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphisme
{
    class Directeur : Chef
    {
        //attributs
        public string Societe { get; set; }

        //constructeur
        public Directeur(string nom, string prenom, string dateDeNaissance, int salaire, string service, string societe)
            : base(nom, prenom, dateDeNaissance, salaire, service)
        {
            Societe = societe;
        }

        //affichage des données du directeur
        public override string Afficher()
        {
            return $"{base.Afficher()} / Société : {Societe}";
        }
    }
}
