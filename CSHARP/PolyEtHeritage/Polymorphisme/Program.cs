﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphisme
{
    class Program
    {
        static void Main(string[] args)
        {
            //création d'un tableau contenant 8 personnes
            Personne[] tabDePersonnes =
            {
                new Employe("John", "Johnson", "01/01/1980", 1000),
                new Employe("Jack", "Jackson", "02/02/1979", 1100),
                new Employe("Bill", "Billy", "03/03/1978", 1200),
                new Employe("Mark", "Markson", "04/04/1977", 1300),
                new Employe("Joe", "Thunder", "04/04/1976", 1400),
                new Chef("Joan", "Billy", "05/05/1975", 2000, "Ventes"),
                new Chef("Melissa", "Bean", "06/06/1974", 1800, "Publicité"),
                new Directeur("Test", "Testman", "12/12/2000", 5000, "Ventes", "Test Inc.")
            };

            //affichage des informations de toutes les personnes
            foreach (Personne personne in tabDePersonnes)
            {
                Console.WriteLine(personne.Afficher());
            }

            //fin de l'application
            Console.ReadLine();
        }
    }
}
