﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphisme
{
    class Chef : Employe
    {
        //attributs
        public string Service { get; set; }

        //constructeur
        public Chef(string nom, string prenom, string dateDeNaissance, int salaire, string service)
            : base(nom, prenom, dateDeNaissance, salaire)
        {
            Service = service;
        }

        //affichage des données du chef
        public override string Afficher()
        {
            return $"{base.Afficher()} / Service : {Service}";
        }
    }
}
