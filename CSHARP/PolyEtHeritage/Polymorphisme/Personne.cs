﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphisme
{
    abstract class Personne
    {
        //attributs
        public string Nom { get; private set; }
        public string Prenom { get; private set; }
        public string DateDeNaissance { get; private set; }

        //constructeur
        public Personne(string nom, string prenom, string dateDeNaissance)
        {
            Nom = nom;
            Prenom = prenom;
            DateDeNaissance = dateDeNaissance;
        }
        
        //affichage des données de la personne
        public virtual string Afficher()
        {
            return $"Nom : {Nom} / Prénom : {Prenom} / Date de naissance : {DateDeNaissance}";
        }
    }
}
