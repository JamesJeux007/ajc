﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heritage
{
    class Camion : Vehicule
    {
        //attributs
        public int CapaciteRemorque { get; set; }

        //constructeur
        public Camion(string code, int puissance, string couleur, string constructeur, int capactiteRemorque)
            : base(code, puissance, couleur, constructeur)
        {
            CapaciteRemorque = capactiteRemorque;
        }

        //affichages des informations du camion
        public override string ToString()
        {
            return $"Camion => {base.ToString()} - Capacité de la remorque : {CapaciteRemorque} kg";
        }
    }
}
