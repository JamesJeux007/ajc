﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heritage
{
    abstract class Vehicule
    {
        //attributs
        public string Code { get; set; }
        public int Puissance { get; set; }
        public string Couleur { get; set; }
        public string Constructeur { get; set; }

        //constructeur
        public Vehicule(string code, int puissance, string couleur, string constructeur)
        {
            Code = code;
            Puissance = puissance;
            Couleur = couleur;
            Constructeur = constructeur;
        }

        //fonction permettant de créer un véhicule en saisissant ses informations
        //public abstract void Saisie();

        //affichage des informations du véhicule
        public override string ToString()
        {
            return $"Code : {Code} - Puissance : {Puissance} - Couleur : {Couleur} - Constructeur : {Constructeur}";
        }
    }
}
