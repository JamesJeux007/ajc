﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heritage
{
    class LesVoitures : IListeVehicules
    {
        //attributs
        public List<Voiture> ListeVoitures { get; set; }

        //constructeur
        public LesVoitures()
        {
            ListeVoitures = new List<Voiture>();
        }

        //moyenne des puissances moyennes
        public void Moyenne()
        {
            //total des puissances
            int totalPuissances = 0;

            //somme de toutes les puissances des voitures
            foreach (Voiture voiture in ListeVoitures)
            {
                totalPuissances += voiture.Puissance;
            }

            //affichage de la moyenne
            Console.WriteLine("Puissance moyenne des voitures = {0}", totalPuissances / ListeVoitures.Count());
        }

        //affichage de toutes les voitures
        public void Affichage()
        {
            //pour chaque voiture dans la liste
            foreach (Voiture voiture in ListeVoitures)
            {
                //affichage des informations
                Console.WriteLine(voiture.ToString());
            }
        }

        //ajout d'une voiture dans la liste
        public void Ajout(Voiture voitureAjoutee)
        {
            ListeVoitures.Add(voitureAjoutee);
            Console.WriteLine($"La voiture {voitureAjoutee.Code} a été ajoutée à la liste.");
        }

        //recherche d'une voiture selon son code
        //public void Recherche(string codeRecherche)
        //{
        //}

        //affichage de la voiture ayant le plus grand nombre de places
        public void Max()
        {

        }
    }
}
