﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heritage
{
    class Moto : Vehicule
    {
        //attributs
        public int Vitesse { get; set; }

        //constructeur
        public Moto(string code, int puissance, string couleur, string constructeur, int vitesse)
            : base(code, puissance, couleur, constructeur)
        {
            Vitesse = vitesse;
        }

        //affichage des informations de la moto
        public override string ToString()
        {
            return $"Moto => {base.ToString()} - Vitesse : {Vitesse} km/h";
        }
    }
}
