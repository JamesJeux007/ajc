﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heritage
{
    class Voiture : Vehicule
    {
        //attributs
        public int NbPlaces { get; set; }

        //constructeur
        public Voiture(string code, int puissance, string couleur, string constructeur, int nbPlaces)
            : base(code, puissance, couleur, constructeur)
        {
            NbPlaces = nbPlaces;
        }

        //affichage des informations de la voiture
        public override string ToString()
        {
            return $"{base.ToString()} - Nombre de places : {NbPlaces}";
        }
    }
}
