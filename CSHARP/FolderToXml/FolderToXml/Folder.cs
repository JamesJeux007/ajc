﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace FolderToXml
{
    [XmlRoot("Folder")]
    public class Folder
    {
        [XmlAttribute("folder_name")]
        public string Name { get; set; }

        [XmlArray("Subfolders")]
        [XmlArrayItem("Subfolder")]
        public Folder[] SubFolders { get; set; }

        [XmlAttribute("nb_subfolders")]
        public int NumSubFolders { get; set; }

        [XmlArray("files")]
        [XmlArrayItem("File")]
        public File[] Files { get; set; }

        [XmlAttribute("nb_files")]
        public int NumFiles { get; set; }

        [XmlElement("folder_path")]
        public string FullPath { get; set; }

        public Folder() { }

        public Folder(string path)
        {
            //getting the information about the given folder
            DirectoryInfo di = new DirectoryInfo(path);

            //saving the folder name and path
            Name = di.Name;
            FullPath = di.FullName;

            //saving the list of subdirectories and their count
            DirectoryInfo[] allFolders = di.GetDirectories();
            NumSubFolders = allFolders.Length;

            //creating the array that will contain all subfolders
            SubFolders = new Folder[NumSubFolders];

            //saving each subfolder in the list
            for (int i = 0; i < NumSubFolders; i++)
            {
                SubFolders[i] = new Folder(allFolders[i].FullName);
            }

            //saving the list of files and their count
            FileInfo[] allFiles = di.GetFiles();
            NumFiles = allFiles.Length;

            //creating the array that will contain all files
            Files = new File[NumFiles];

            //saving each file in the list
            for (int i = 0; i < NumFiles; i++)
            {
                Files[i] = new File(allFiles[i]);
            }
        }

        /*
        static void SavingListAndCount<L, I>(I[] info, ref L[] list, ref int count)
        {
            //saving the count
            count = info.Length;

            //creating the array that will contain all instances
            list = new L[count];

            //saving each instance in the list
            for (int i = 0; i < count; i++)
            {
                list[i] = new L(info[i]);
            }
        }
        */
    }
}
