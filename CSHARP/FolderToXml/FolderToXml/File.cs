﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FolderToXml
{
    public class File
    {
        [XmlElement("filename")]
        public string Name { get; set; }

        [XmlAttribute("file_path")]
        public string FullPath { get; set; }

        [XmlAttribute("filesize")]
        public long Size { get; set; }

        [XmlAttribute("extension")]
        public string Extension { get; set; }

        public File() { }

        public File(FileInfo file)
        {
            //saving all information based in the FileInfo given
            Name = file.Name;
            FullPath = file.FullName;
            Size = file.Length;
            Extension = file.Extension;
        }
    }
}
