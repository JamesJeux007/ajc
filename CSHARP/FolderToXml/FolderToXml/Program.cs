﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FolderToXml
{
    class Program
    {
        static void Main(string[] args)
        {
            //saving folder path
            string folderpath = @"C:\Users\utilisateur\Documents\Formation AJC";

            //creating a new folder
            Folder folderToXMLize = new Folder(folderpath);

            //creating XML serializer
            XmlSerializer xs = new XmlSerializer(typeof(Folder));

            //writing XML file
            using (StreamWriter writer = new StreamWriter("XMLed_Folder.xml"))
            {
                xs.Serialize(writer, folderToXMLize);
            }

            //telling the user the file has been created
            Console.WriteLine($"Le fichier XML a correctement été généré pour le chemin :\n{folderpath}");

            //end of Main()
            Console.ReadLine();
        }
    }
}
