﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesAbstraites
{
    class Patron : Employe
    {
        private static int CA = 100000;

        public double Pourcentage { get; set; }

        public Patron(int matricule, string nom, string prénom, string dateDeNaissance, double pourcentage)
            : base(matricule, nom, prénom, dateDeNaissance)
        {
            Pourcentage = pourcentage;
        }

        public override int GetSalaire()
        {
            double salaire = CA * (Pourcentage / 100);

            return Convert.ToInt32(salaire);
        }

        public static void ChangeCA(int newCA)
        {
            CA = newCA;
        }
    }
}
