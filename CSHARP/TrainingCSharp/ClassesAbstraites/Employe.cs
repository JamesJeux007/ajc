﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesAbstraites
{
    abstract class Employe
    {
        public int Matricule { get; set; }
        public string Nom { get; set; }
        public string Prénom { get; set; }
        public string DateDeNaissance { get; set; }

        public Employe(int matricule, string nom, string prénom, string dateDeNaissance)
        {
            Matricule = matricule;
            Nom = nom;
            Prénom = prénom;
            DateDeNaissance = dateDeNaissance;
        }

        public new string ToString()
        {
            return nameof(Matricule) + " : " + Matricule + ", " +
                nameof(Nom) + " : " + Nom + ", " +
                nameof(Prénom) + " : " + Prénom + ", " +
                nameof(DateDeNaissance) + " : " + DateDeNaissance;
        }

        public abstract int GetSalaire();
    }
}
