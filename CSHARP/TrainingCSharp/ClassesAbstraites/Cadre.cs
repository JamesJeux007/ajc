﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesAbstraites
{
    class Cadre : Employe
    {
        public int Indice { get; set; }

        public Cadre(int matricule, string nom, string prénom, string dateDeNaissance, int indice)
            : base(matricule, nom, prénom, dateDeNaissance)
        {
            Indice = indice;
        }

        public override int GetSalaire()
        {
            int salaire = 0;

            switch (Indice)
            {
                case 1:
                    salaire = 13000;
                    break;
                case 2:
                    salaire = 15000;
                    break;
                case 3:
                    salaire = 17000;
                    break;
                case 4:
                    salaire = 20000;
                    break;
            }

            return salaire;
        }
    }
}
