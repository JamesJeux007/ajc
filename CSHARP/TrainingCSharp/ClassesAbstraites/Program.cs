﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesAbstraites
{
    class Program
    {
        static void Main(string[] args)
        {
            //création d'un ouvrier
            Ouvrier Anthony = new Ouvrier(87, "Bastien", "Anthony", "14/12/1990", 3);

            //affichage des informations de l'ouvrier
            AffichInfosEmployé(Anthony);

            //création d'un cadre
            Cadre Valentin = new Cadre(25, "Bastien", "Valentin", "28/05/1994", 2);

            //affichage des informations du cadre
            AffichInfosEmployé(Valentin);

            //création d'un patron
            Patron Baptiste = new Patron(2, "Bastien", "Baptiste", "06/07/2007", 25);

            //affichage des informations du patron
            AffichInfosEmployé(Baptiste);

            //modification du chiffre d'affaires
            Patron.ChangeCA(200000);

            //affichage des nouvelles infomations du patron
            AffichInfosEmployé(Baptiste);

            //fin de l'application
            Console.ReadLine();
        }

        //fonction affichant le salaire donné
        static void AffichSalaire(int salaire)
        {
            Console.WriteLine("Le salaire de l'employé est de {0} DH.", salaire);
        }

        //fonction affichant les informations de l'employé donné
        static void AffichInfosEmployé(Employe employé)
        {
            Console.WriteLine(employé.ToString());
            AffichSalaire(employé.GetSalaire());
            Console.WriteLine();
        }
    }
}
