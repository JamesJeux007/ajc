﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesAbstraites
{
    class Ouvrier : Employe
    {
        private static int SMIG = 2500;
        public int Ancienneté { get; set; }

        public Ouvrier(int matricule, string nom, string prénom, string dateDeNaissance, int ancienneté)
            : base(matricule, nom, prénom, dateDeNaissance)
        {
            Ancienneté = ancienneté;
        }

        public override int GetSalaire()
        {
            int salaire = SMIG + (Ancienneté * 100);

            if(salaire > SMIG * 2)
            {
                salaire = SMIG * 2;
            }

            return salaire;
        }
    }
}
