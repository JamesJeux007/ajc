﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TrainingCSharp
{
    class ExerciceAfficheLettre
    {
        public static void Affichage()
        {
            //demande de la lettre à afficher
            Console.WriteLine("Quelle lettre souhaitez-vous afficher ?");
            string lettre = Console.ReadLine();

            //déclaration de la variable d'erreur
            bool entréeCorrecte = false;

            //création d'une expression pour vérifier l'entrée
            Regex reg = new Regex("^[a-zA-Z]$");

            do //tant que l'entrée est incorrecte
            {
                //l'entrée n'est composée que d'un seul caractère et celui-ci est une lettre
                if (reg.IsMatch(lettre))
                {
                    entréeCorrecte = true;
                }

                //message d'erreur si l'entrée est incorrecte
                if (!entréeCorrecte)
                {
                    Console.WriteLine("Veuillez entrer une lettre.");
                    lettre = Console.ReadLine();
                }

            } while (!entréeCorrecte);

            //mise en majuscule de la lettre
            lettre = lettre.ToUpper();

            //la lettre à afficher est X
            if (lettre == "X")
            {
                //demande de la taille voulue
                Console.WriteLine("De quelle taille souhaitez-vous la lettre ?");
                string tailleChaine = Console.ReadLine();

                //initialisation de l'erreur et de la variable contenant la taille
                entréeCorrecte = false;
                int taille = 0;

                do //tant que la taille n'est pas correcte
                {
                    //l'entrée est bien un chiffre entier
                    if (Int32.TryParse(tailleChaine, out taille))
                    {
                        //l'entrée est comprise entre 1 et 10
                        if (taille >= 1 && taille <= 10)
                        {
                            entréeCorrecte = true;
                        }
                    }

                    //message d'erreur si l'entrée est incorrecte
                    if (!entréeCorrecte)
                    {
                        Console.WriteLine("Veuillez entrer une nombre entre 1 et 10.");
                        tailleChaine = Console.ReadLine();
                    }

                } while (!entréeCorrecte);
            }
        }
    }
}
