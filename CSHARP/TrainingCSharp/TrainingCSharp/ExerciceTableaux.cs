﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingCSharp
{
    class ExerciceTableaux
    {
        public static void Affichage()
        {
            //tableau imbriqués
            int[][] tabManuel = new int[3][];

            tabManuel[0] = new int[4] { 0, -1, 11, 3 };
            tabManuel[1] = new int[4] { 4, 110, -60, 7 };
            tabManuel[2] = new int[4] { 8, -94, 5, 2 };

            //tableau deux dimentions
            //int[,] tabDeTab = new int[,] { { 0, 1, 2, 3 }, { 4, 5, 6, 7 }, { 8, 9, 10, 11 } };

            //affichage du tableau imbriqué
            AffichTabAvecSous(tabManuel, "Affichage du tableau de départ");

            //déconstruction du tableau dans un grand tableau
            int[] tabDeTri = new int[tabManuel.Length * tabManuel[0].Length];

            //initialisation de l'index actuel du grand tableau
            int indexGrandTab = 0;

            //pour chaque sous-tableau
            for (int i = 0; i < tabManuel.Length; i++)
            {
                //pour chaque nombre par sous-tableau
                for (int j = 0; j < tabManuel[i].Length; j++)
                {
                    //ajout de la valeur dans le grand tableau
                    tabDeTri[indexGrandTab] = tabManuel[i][j];

                    //index suivant
                    indexGrandTab++;
                }
            }

            //affichage du tableau de tri
            AffichTab(tabDeTri, "Tableaux imbriqués enrgistrés dans un seul grand tableau");
            Console.WriteLine();

            //initialisation des variables de tri et d'échange
            int indexTri = 0;
            int temp = 0;

            //s'il faut échanger le premier index
            if (tabDeTri[0] < tabDeTri[1])
            {
                //variable temporaire
                temp = tabDeTri[0];

                //tri des données
                tabDeTri[0] = tabDeTri[1];
                tabDeTri[1] = temp;
            }

            //tri du tableau (commence par 1 pour ne pas sortir du tableau lors de la vérification)
            for (int i = 1; i < tabDeTri.Length; i++)
            {
                //enregistrement de la variable d'index vérifié
                indexTri = i;

                //tant que la valeur actuelle n'est pas triée
                while (tabDeTri[indexTri - 1] < tabDeTri[indexTri] && indexTri > 1)
                {
                    //variable temporaire
                    temp = tabDeTri[indexTri - 1];

                    //tri des données
                    tabDeTri[indexTri - 1] = tabDeTri[indexTri];
                    tabDeTri[indexTri] = temp;

                    //index précédent
                    indexTri--;
                }

                //s'il ne reste plus que le premier index à échanger
                if (indexTri == 1 && tabDeTri[indexTri - 1] < tabDeTri[indexTri])
                {
                    //variable temporaire
                    temp = tabDeTri[indexTri - 1];

                    //tri des données
                    tabDeTri[indexTri - 1] = tabDeTri[indexTri];
                    tabDeTri[indexTri] = temp;
                }
            }

            //affichage du tableau de tri trié
            AffichTab(tabDeTri, "Tableau trié");
            Console.WriteLine();

            //mise à zéro de la variable parcourant le tableau de tri
            indexGrandTab = 0;

            //pour chaque sous-tableau
            for (int i = 0; i < tabManuel.Length; i++)
            {
                //pour chaque nombre par sous-tableau
                for (int j = 0; j < tabManuel[i].Length; j++)
                {
                    //remplacement de la valeur dans le sous tableau
                    tabManuel[i][j] = tabDeTri[indexGrandTab];

                    //index suivant
                    indexGrandTab++;
                }
            }

            AffichTabAvecSous(tabManuel, "Affichage du tableau trié");
        }

        //fonction affichant un tableau contenant un sous-tableau
        static void AffichTabAvecSous(int[][] tabAvecSous, string titre)
        {
            //initialisation du séparateur des lignes
            string separateur = "---------------------";

            //affichage du tableau
            Console.WriteLine("{0} :", titre);
            Console.WriteLine(separateur);

            //pour chaque sous-tableau
            foreach (int[] tab in tabAvecSous)
            {
                /*
                //initialisation de l'affichage
                string affichLigne = "| ";

                //pour chaque nombre par sous-tableau
                foreach (int sousTab in tab)
                {
                    //enregistrement de l'affichage
                    affichLigne += sousTab.ToString() + " | ";
                }

                //affichage séparateur
                Console.WriteLine(affichLigne);
                Console.WriteLine(separateur);
                */

                //affichage du tableau en une ligne avec séparateur
                AffichTab(tab);
                Console.WriteLine(separateur);
            }

            //saut de ligne après le tableau
            Console.WriteLine();
        }

        //fonction affichant un tableau une dimention en une ligne
        static void AffichTab(int[] tableau, string titre = "")
        {

            //affichage du titre si besoin
            if (titre != "")
            {
                Console.WriteLine("{0} :", titre);
            }

            //initialisation de la variable d'affichage
            string affichage = "[";

            //on parcourt le tableau
            foreach (int nombre in tableau)
            {
                //enregistrement du chiffre avec exception pour le premier
                if (affichage == "[")
                {
                    affichage += nombre.ToString();
                }
                else
                {
                    affichage += ", " + nombre.ToString();
                }
            }

            //fin de l'affichage
            affichage += "]";

            //affichage du tableau en une ligne
            Console.WriteLine(affichage);
        }
    }
}
