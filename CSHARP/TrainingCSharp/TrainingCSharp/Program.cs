﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            //création d'un étudiant
            Etudiant Anthony = new Etudiant("Bastien", "Anthony", 2019, "C#");

            //présentation de l'étudiant
            Console.WriteLine(Anthony.présentation());

            //création d'une personne
            Personne John = new Personne("Johnson", "John");

            //présentation de la personne
            Console.WriteLine(John.présentation());

            //fin du programme, input pour fermer la console
            Console.ReadLine();
        }
    }
}
