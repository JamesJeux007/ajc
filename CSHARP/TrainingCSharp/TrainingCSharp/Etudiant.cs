﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingCSharp
{
    class Etudiant : Personne
    {
        public int Année { get; set; }
        public string Spécialité { get; set; }

        public Etudiant(string nom, string prénom, int année, string spécialité) 
            : base(nom, prénom)
        {
            Année = année;
            Spécialité = spécialité;
        }

        public new string présentation()
        {
            return base.présentation().Replace(".", ", étudiant en " + Spécialité + ".");
        }
    }
}
