﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingCSharp
{
    class Personne
    {
        protected string Nom { get; set; }

        private string prénom;

        public string Prénom
        {
            get { return prénom; }
            set {
                if (value != "Ajc" && value != "Formation")
                {
                    prénom = value;
                }
            }
        }

        public Personne()
        {

        }

        public Personne(string nom, string prénom)
        {
            Nom = nom;
            Prénom = prénom;
        }

        public string présentation()
        {
            return "Je m'appelle " + Prénom + " " + Nom + ".";
        }
    }
}
