﻿using FoodTruckCore.DAL;
using FoodTruckCore.Model;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruck.ViewModel
{
    class Accueil_ViewModel : BindableBase
    {
        #region Attributs

        private ObservableCollection<Actualite> ListeActus { get; set; }

        private Actualite _actuAffichee;
        public Actualite ActuAffichee
        {
            get { return _actuAffichee; }
            set { SetProperty(ref _actuAffichee, value); }
        }

        private int NumActuAffichee { get; set; }

        public List<Produit> PlusVendus { get; set; }

        #endregion

        #region Commandes

        private DelegateCommand _command_ActuSuivante;
        public DelegateCommand ActuSuivante
        {
            get { return _command_ActuSuivante; }
            set { _command_ActuSuivante = value; }
        }

        private DelegateCommand _command_ActuPrecedente;
        public DelegateCommand ActuPrecedente
        {
            get { return _command_ActuPrecedente; }
            set { _command_ActuPrecedente = value; }
        }

        #endregion

        #region Constructeur

        public Accueil_ViewModel()
        {
            //obtention de la liste d'actualités dans la base de données
            ListeActus = new ObservableCollection<Actualite>(Actualite_DAL.DonneListeActu());

            //génération d'un nombre aléatoire compris entre 1 et le nombre total d'actualités
            NumActuAffichee = (new Random()).Next(ListeActus.Count);

            //sélection d'une actualité aléatoirement
            ActuAffichee = ListeActus[NumActuAffichee];

            //obtention de la liste des produits les plus vendus
            PlusVendus = Produit_DAL.DonnePlusVendus();

            //commandes
            _command_ActuSuivante = new DelegateCommand(Do_ActuSuivante);
            _command_ActuPrecedente = new DelegateCommand(Do_ActuPrecedente);
        }

        #endregion

        #region Méthodes

        //affiche l'actualité suivante
        public void Do_ActuSuivante()
        {
            //si on est à la fin de la liste des actualité
            if (++NumActuAffichee == ListeActus.Count)
            {
                //on affiche la première
                NumActuAffichee = 0;
            }

            //modification de l'actualité affichée
            ActuAffichee = ListeActus[NumActuAffichee];
        }

        //affiche l'actualité précédente
        public void Do_ActuPrecedente()
        {
            //si on est à la première actualité
            if (--NumActuAffichee == -1)
            {
                //on affiche la dernière
                NumActuAffichee = ListeActus.Count - 1;
            }

            //modification de l'actualité affichée
            ActuAffichee = ListeActus[NumActuAffichee];
        }

        #endregion
    }
}
