USE [FOODTRUCK]
GO
/****** Object:  User [FoodTruckUser]    Script Date: 18/09/2019 17:05:43 ******/
CREATE USER [FoodTruckUser] FOR LOGIN [FoodTruckUser] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [FoodTruckUser]
GO
/****** Object:  Table [dbo].[ACTUALITES]    Script Date: 18/09/2019 17:05:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACTUALITES](
	[ID_ACTUALITE] [int] IDENTITY(1,1) NOT NULL,
	[TITRE] [varchar](255) NOT NULL,
	[DESCRIPTION] [text] NOT NULL,
	[URL_IMAGE] [varchar](255) NOT NULL,
	[DATE_DEBUT] [datetime] NOT NULL,
	[DATE_FIN] [datetime] NOT NULL,
	[LINK] [varchar](255) NOT NULL,
	[ID_UTILISATEUR] [int] NOT NULL,
	[ACTIF] [bit] NOT NULL,
 CONSTRAINT [PK_ACTUALITES] PRIMARY KEY CLUSTERED 
(
	[ID_ACTUALITE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PRODUIT]    Script Date: 18/09/2019 17:05:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUIT](
	[ID_PRODUIT] [int] IDENTITY(1,1) NOT NULL,
	[ID_FAMILLE_REPAS] [int] NOT NULL,
	[LIBELLE_PRODUIT] [varchar](30) NOT NULL,
	[DESCRIPTION] [text] NULL,
	[NOMBRE_DE_VENTE] [int] NOT NULL,
	[PRIX] [numeric](18, 2) NOT NULL,
	[URL_IMAGE] [varchar](255) NOT NULL,
	[STOCK] [int] NOT NULL,
	[LMMJVSD] [varchar](7) NULL,
	[MOYENNE_NOTE] [float] NULL,
	[UNITE] [varchar](10) NULL,
 CONSTRAINT [PK_PRODUIT] PRIMARY KEY CLUSTERED 
(
	[ID_PRODUIT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ACTUALITES] ON 

INSERT [dbo].[ACTUALITES] ([ID_ACTUALITE], [TITRE], [DESCRIPTION], [URL_IMAGE], [DATE_DEBUT], [DATE_FIN], [LINK], [ID_UTILISATEUR], [ACTIF]) VALUES (1, N'Manger vegan à Barcelone', N'Barcelone est une ville très connue pour sa gastronomie, notamment pour ses tapas. Et ces derniers existent en de nombreuses versions depuis plusieurs années.', N'https://www.vegactu.com/wp-content/uploads/2019/05/flaxkale.jpg', CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2029-12-31T00:00:00.000' AS DateTime), N'https://www.vegactu.com/actualite/style-de-vie/manger-vegan-a-barcelone-28405/', 1, 1)
INSERT [dbo].[ACTUALITES] ([ID_ACTUALITE], [TITRE], [DESCRIPTION], [URL_IMAGE], [DATE_DEBUT], [DATE_FIN], [LINK], [ID_UTILISATEUR], [ACTIF]) VALUES (2, N'Attention au sucre', N'Il faut faire attention ....', N'https://www.vegactu.com/wp-content/uploads/2019/05/flaxkale.jpg', CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2029-12-31T00:00:00.000' AS DateTime), N'https://www.vegactu.com/actualite/style-de-vie/manger-vegan-a-barcelone-28405/', 1, 1)
INSERT [dbo].[ACTUALITES] ([ID_ACTUALITE], [TITRE], [DESCRIPTION], [URL_IMAGE], [DATE_DEBUT], [DATE_FIN], [LINK], [ID_UTILISATEUR], [ACTIF]) VALUES (3, N'Mangez des 5 fruits et légumes par jour ', N'Il faut faire attention à votre alimentation....', N'https://www.vegactu.com/wp-content/uploads/2019/05/flaxkale.jpg', CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2029-12-31T00:00:00.000' AS DateTime), N'https://www.vegactu.com/actualite/style-de-vie/manger-vegan-a-barcelone-28405/', 1, 1)
INSERT [dbo].[ACTUALITES] ([ID_ACTUALITE], [TITRE], [DESCRIPTION], [URL_IMAGE], [DATE_DEBUT], [DATE_FIN], [LINK], [ID_UTILISATEUR], [ACTIF]) VALUES (4, N'Faite du sport au moins trois fois par semaine ', N'Le sport est important pour entretenir la santé ....', N'https://www.vegactu.com/wp-content/uploads/2019/05/flaxkale.jpg', CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2029-12-31T00:00:00.000' AS DateTime), N'https://www.vegactu.com/actualite/style-de-vie/manger-vegan-a-barcelone-28405/', 1, 1)
SET IDENTITY_INSERT [dbo].[ACTUALITES] OFF
SET IDENTITY_INSERT [dbo].[PRODUIT] ON 

INSERT [dbo].[PRODUIT] ([ID_PRODUIT], [ID_FAMILLE_REPAS], [LIBELLE_PRODUIT], [DESCRIPTION], [NOMBRE_DE_VENTE], [PRIX], [URL_IMAGE], [STOCK], [LMMJVSD], [MOYENNE_NOTE], [UNITE]) VALUES (3, 1, N'Burger', N'Un bon burger.', 0, CAST(5.00 AS Numeric(18, 2)), N'burger.jpg', 5, NULL, 5, NULL)
INSERT [dbo].[PRODUIT] ([ID_PRODUIT], [ID_FAMILLE_REPAS], [LIBELLE_PRODUIT], [DESCRIPTION], [NOMBRE_DE_VENTE], [PRIX], [URL_IMAGE], [STOCK], [LMMJVSD], [MOYENNE_NOTE], [UNITE]) VALUES (4, 1, N'Pizza', N'Pizza Marguerita', 0, CAST(11.00 AS Numeric(18, 2)), N'pizza.jpg', 5, NULL, 4.5, NULL)
INSERT [dbo].[PRODUIT] ([ID_PRODUIT], [ID_FAMILLE_REPAS], [LIBELLE_PRODUIT], [DESCRIPTION], [NOMBRE_DE_VENTE], [PRIX], [URL_IMAGE], [STOCK], [LMMJVSD], [MOYENNE_NOTE], [UNITE]) VALUES (5, 3, N'Salade', N'Pleine de bonnes choses', 0, CAST(4.00 AS Numeric(18, 2)), N'salade.jpg', 14, NULL, 4.88, NULL)
SET IDENTITY_INSERT [dbo].[PRODUIT] OFF
ALTER TABLE [dbo].[ACTUALITES] ADD  CONSTRAINT [DF_ACTUALITES_ACTIF]  DEFAULT ((1)) FOR [ACTIF]
GO
ALTER TABLE [dbo].[PRODUIT] ADD  CONSTRAINT [DF_PRODUIT_NOMBRE_DE_VENTE]  DEFAULT ((0)) FOR [NOMBRE_DE_VENTE]
GO
