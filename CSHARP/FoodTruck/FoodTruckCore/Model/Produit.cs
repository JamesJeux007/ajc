﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckCore.Model
{
    public class Produit
    {
        public int Id_Produit { get; set; }
        public string Libelle_Produit { get; set; }
        public string Description { get; set; }
        public int Nombre_de_vente { get; set; }
        public decimal Prix { get; set; }
        public string Url_image { get; set; }
        public int Stock { get; set; }
        public string LMMJVSD { get; set; }
        public double Moyenne_note { get; set; }
        public string Unite { get; set; }

        public Produit() { }

    }
}
