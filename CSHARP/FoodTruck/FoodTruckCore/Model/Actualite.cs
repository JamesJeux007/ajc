﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckCore.Model
{
    public class Actualite
    {
        public int Id_Actualite { get; set; }
        public string Titre { get; set; }
        public string Description { get; set; }
        public string Url_Image { get; set; }
        public DateTime Date_Debut { get; set; }
        public DateTime Date_Fin { get; set; }
        public string Link { get; set; }
        public bool Actif { get; set; }

        public Actualite() { }

        public override string ToString()
        {
            return $"{Id_Actualite} | {Titre} - {Description} - {Url_Image} - " +
                $"{Date_Debut.ToString("dd/MM/yyyy")} - {Date_Fin.ToString("dd/MM/yyyy")} - {Link} - {Actif}";
        }
    }
}
