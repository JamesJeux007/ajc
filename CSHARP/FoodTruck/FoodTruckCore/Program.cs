﻿using FoodTruckCore.DAL;
using FoodTruckCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FoodTruckCore
{
    class Program
    {
        static void Main(string[] args)
        {
            //tests
            var listActu = Actualite_DAL.DonneListeActu();

            foreach (Actualite actu in listActu)
            {
                Console.WriteLine(actu.ToString());
            }

            //fin de l'application
            Console.ReadLine();
        }
    }
}
