﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckCore.DAL
{
    public class BaseDAL
    {
        private SqlConnection _connexion { get; set; }
        public SqlCommand Requete { get; set; }
        public BaseDAL()
        {
            //nouvelle connexion
            _connexion = new SqlConnection();

            //connexion à la base de données
            _connexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConnexionFoodTruck"].ConnectionString;
            _connexion.Open();

            //nouvelle requête
            Requete = new SqlCommand();

            //requête dans la BDD
            Requete.Connection = _connexion;
        }

        //fermeture de la connexion
        public void FermeConnexion()
        {
            _connexion.Close();
        }
    }
}
