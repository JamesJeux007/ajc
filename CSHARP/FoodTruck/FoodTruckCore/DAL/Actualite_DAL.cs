﻿using FoodTruckCore.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckCore.DAL
{
    public static class Actualite_DAL
    {
        //obtention de la liste de toutes les actualités
        public static List<Actualite> DonneListeActu()
        {
            //création de la liste de retour vide
            List<Actualite> listeActu = new List<Actualite>();

            //lien avec la base de données
            BaseDAL dal = new BaseDAL();

            //création de la requête
            dal.Requete.CommandText =
                "SELECT ID_ACTUALITE, TITRE, DESCRIPTION, URL_IMAGE, DATE_DEBUT, DATE_FIN, LINK, ACTIF " +
                "FROM ACTUALITES;";

            //exécution de la requête
            using (SqlDataReader resultat = dal.Requete.ExecuteReader())
            {
                //pour chaque ligne
                while (resultat.Read())
                {
                    //ajout de l'actualité dans la liste
                    listeActu.Add(new Actualite()
                    {
                        Id_Actualite = resultat.GetInt32(0),
                        Titre = resultat.GetString(1),
                        Description = resultat.GetString(2),
                        Url_Image = resultat.GetString(3),
                        Date_Debut = resultat.GetDateTime(4),
                        Date_Fin = resultat.GetDateTime(5),
                        Link = resultat.GetString(6),
                        Actif = resultat.GetBoolean(7)
                    });
                }
            }

            //fermeture de la connexion
            dal.FermeConnexion();

            //renvoi de la liste des actualités
            return listeActu;
        }
    }
}
