﻿using FoodTruckCore.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckCore.DAL
{
    public static class Produit_DAL
    {
        //obtention des 3 produits les plus vendus
        public static List<Produit> DonnePlusVendus()
        {
            //création de la liste de retour vide
            List<Produit> plusVendus = new List<Produit>();

            //lien avec la BDD
            BaseDAL dal = new BaseDAL();

            //création de la requête
            dal.Requete.CommandText =
                "SELECT TOP 3 LIBELLE_PRODUIT, PRIX, URL_IMAGE, MOYENNE_NOTE " +
                "FROM PRODUIT " +
                "ORDER BY MOYENNE_NOTE DESC;";

            //exécution de la requête
            using (SqlDataReader resultat = dal.Requete.ExecuteReader())
            {
                //pour toutes les lignes du tableau
                while (resultat.Read())
                {
                    //ajout du produit dans la liste
                    plusVendus.Add(new Produit()
                    {
                        Libelle_Produit = resultat.GetString(0),
                        Prix = resultat.GetDecimal(1),
                        Url_image = resultat.GetString(2),
                        Moyenne_note = resultat.GetDouble(3)
                    });
                }
            }

            //retour de la liste des produits les plus vendus
            return plusVendus;
        }
    }
}
